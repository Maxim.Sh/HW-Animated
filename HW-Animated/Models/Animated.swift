//
//  Animated.swift
//  HW-Animated
//
//  Created by Максим Шкрябин on 07.07.2023.
//

struct Animated {
    
    var title: String
    var curve: String
    
    var force: Double
    var delay: Double
    var duration: Double
    
    static func getDefaultAnimation() -> Animated {
        return Animated(
            title: titles[0],
            curve: curves[0],
            force: 1,
            delay: 0.5,
            duration: 1
        )
    }
}

let titles = [
    "pop",
    "slideLeft",
    "slideRight",
    "slideDown",
    "slideUp",
    "squeezeLeft",
    "squeezeRight",
    "squeezeDown",
    "squeezeUp",
    "fadeIn",
    "fadeOut",
    "fadeOutIn",
    "fadeInLeft",
    "fadeInRight",
    "fadeInDown",
    "fadeInUp",
    "oomIn",
    "zoomOut",
    "fall",
    "shake",
    "flipX",
    "flipY",
    "morph",
    "squeeze",
    "flash",
    "wobble",
    "swing"
]


let curves = [
    "easeIn",
    "easeOut",
    "easeInOut",
    "linear",
    "spring",
    "easeInSine",
    "easeOutSine",
    "easeInOutSine",
    "easeInQuad",
    "easeOutQuad",
    "easeInOutQuad",
    "easeInCubic",
    "easeOutCubic",
    "easeInOutCubic",
    "easeInQuart",
    "easeOutQuart",
    "easeInOutQuart",
    "easeInQuint",
    "easeOutQuint",
    "easeInOutQuint",
    "easeInExpo",
    "easeOutExpo",
    "easeInOutExpo",
    "easeInCirc",
    "easeOutCirc",
    "easeInOutCirc",
    "easeInBack",
    "easeOutBack",
    "easeInOutBack"
]
