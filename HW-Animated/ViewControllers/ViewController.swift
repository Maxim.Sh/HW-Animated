//
//  ViewController.swift
//  HW-Animated
//
//  Created by Максим Шкрябин on 06.07.2023.
//

import UIKit
import SpringAnimation

final class ViewController: UIViewController {

    private var animation = false
    private var currentAnimetion = Animated.getDefaultAnimation()
    private var nextAnimation = Animated.getDefaultAnimation()
    
    @IBOutlet var startButton: UIButton!
    @IBOutlet var figureAnimation: SpringView!
    @IBOutlet var textLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAnimate()
    }

    @IBAction func startAnimation(_ sender: UIButton) {
        
        figureAnimation.animation = nextAnimation.title
        figureAnimation.curve = nextAnimation.curve
        figureAnimation.force = nextAnimation.force
        figureAnimation.delay = nextAnimation.delay
        figureAnimation.duration = nextAnimation.duration

        figureAnimation.animate()
        
        currentAnimetion = nextAnimation
        nextAnimation = Animated(
            title: titles[Int.random(in: 0..<titles.count)],
            curve: curves[Int.random(in: 0..<curves.count)],
            force: Double.random(in: 0.5...2),
            delay: Double.random(in: 0.3...0.7),
            duration: Double.random(in: 1...2))
        
        setupAnimate()
    }
    
    private func setupAnimate() {
        textLabel.text = """
                        preset: \(currentAnimetion.title)
                        curve: \(currentAnimetion.curve)
                        force: \(String(format: "%.2f", currentAnimetion.force))
                        delay: \(String(format: "%.2f", currentAnimetion.delay))
                        duration: \(String(format: "%.2f", currentAnimetion.duration))
                        """
        startButton.setTitle("Next animation: \(nextAnimation.title)", for: .normal)
    }
    
    
}

